#include "ModuleParameterisation.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Box.hh"


ModuleParameterisation::ModuleParameterisation(G4double spacing_cell, G4int sideArray_X, G4int sideArray_Y, G4double verticalOffset)
{
    fSpacingCell =  spacing_cell;
    fSideArrayX  =  sideArray_X;
    fSideArrayY  =  sideArray_Y;
    fVerticalOffset = verticalOffset;
}


ModuleParameterisation::~ModuleParameterisation() 
{;}


void ModuleParameterisation::ComputeTransformation (const G4int copyNo, G4VPhysicalVolume* physVol) const
{ 
    // Row and column corresponding to this copy (number copyNo)
    G4int row    = copyNo / fSideArrayX;  // integer division!
    G4int column = copyNo % fSideArrayX;  // modulo!
    
    G4double xCell;
    G4double yCell;
    
    xCell = column*fSpacingCell - (fSideArrayX-1)*fSpacingCell/2.;
    yCell = row*fSpacingCell    - (fSideArrayY-1)*fSpacingCell/2.;
    
    physVol->SetTranslation(G4ThreeVector(xCell, yCell, fVerticalOffset));

}



