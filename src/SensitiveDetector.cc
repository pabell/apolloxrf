#include "SensitiveDetector.hh"
#include "DetectorHit.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4StepPoint.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include <iostream>

// Constructor.
SensitiveDetector::SensitiveDetector(G4String SDuniqueName) :
G4VSensitiveDetector(SDuniqueName),
hitsCollectionID(-1)
{
    collectionName.insert(SDuniqueName);
}



// Destructor
SensitiveDetector::~SensitiveDetector() 
{
}



void SensitiveDetector::Initialize(G4HCofThisEvent* hitsCollEvent)
{
    // This method is called at each new event, and it gets the HCofThisEvents
    // for the event.
    hitsCollection = new DetectorHitsCollection(SensitiveDetectorName, collectionName[0]);
    
    hitsCollectionID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    hitsCollEvent->AddHitsCollection(hitsCollectionID, hitsCollection);
    
}


G4bool SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory* ROhist) 
{
	// This method is called when a particle goes through a sensitive 
	// detector. It possibly creates a new hit, and add its to the collection.
	// It eventually returns a boolean value. 
	// The arguments it gets are the G4Step and the G4TouchableHistory.
	//
	// The touchable history that we get in this method contains the full information about the 
	// genealogy of the current volume in the read-out geometry. One can use the touchable to 
	// access the information on the position/rotation of a given volume.

//	if(!ROhist) return false;

    // Retrieve the energy deposited from the step
//    G4double energyDeposit = step -> GetTotalEnergyDeposit();
//    G4double energy_threshold = 0.05 *keV;
//    if (energyDeposit < energy_threshold) return false;
    
    G4Track* track = step -> GetTrack();

    // Get pre-post step volumes (check if crossing boundaries)
    G4StepPoint* thePrePoint = step->GetPreStepPoint();
    G4StepPoint* thePostPoint = step->GetPostStepPoint();

    G4int icell = 0;
    
    if (thePrePoint->GetStepStatus() == fGeomBoundary) // If this is the first step in volume
    {
        G4double energy = step->GetTotalEnergyDeposit();
//        if (energy >= 400*eV)
        {
            //G4cout << "Energy deposit " << energy/eV << G4endl;
        // Get position
        G4ThreeVector position = track -> GetPosition();
        G4double position_x = position.x();
        G4double position_y = position.y();
        // G4cout << "x " << position_x/cm << " y " << position_y/cm << G4endl;
        // Now we create a new hit object, corresponding to the cell, and fill it with values to be stored
        DetectorHit* hit = new DetectorHit();
        hit -> SetX(position_x);
        hit -> SetY(position_y);
        icell = hitsCollection -> insert(hit);
        }
        return true;
    }
    else
    {
        return false;
    }
   

    
    
}


void SensitiveDetector::EndOfEvent(G4HCofThisEvent* hitsCollEvent) 
{
//	// Method called at the end of an event.
//	if(hitsCollectionID < 0) {
//		hitsCollectionID =
//		G4SDManager::GetSDMpointer() -> GetCollectionID(hitsCollection);
//	}
//	// Finally, the hits collection just defined (with its ID and its hits) 
//	// is registered to the HCOfThisEvent of the current event. 
//	hitsCollEvent -> AddHitsCollection(hitsCollectionID, hitsCollection);
}
