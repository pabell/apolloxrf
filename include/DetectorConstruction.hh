#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class SensitiveDetector;
class G4VPVParameterisation;

#include "G4VUserDetectorConstruction.hh"

// Mandatory user class that defines the detector used in the
// simulation, its geometry and its materials.
// Derived from the G4VUserDetectorConstruction initialisation 
// abstract base class.

class DetectorConstruction : public G4VUserDetectorConstruction
{
	public:
		DetectorConstruction();		// Constructor
		~DetectorConstruction();	// Destructor

	private:
        // Method to construct the detector
        G4VPhysicalVolume* Construct();
        // Method to construct the sensitive detector
        void ConstructSDandField();
        // Method to define the materials
        void DefineMaterials();
        // Method to read the values of parameters from file
        void DefineParameters();

	public:
		// Get methods
		// Method to get the world physical volume
	    const G4VPhysicalVolume* GetWorld()     {return experimentalHall_phys;};

	    // Method to get the world dimensions
		G4double GetWorldSide()               	{return worldSide;};

		// Method to get the collimator physical volume
	    const G4VPhysicalVolume* GetColl()  	{return coll_phys;};

	    // Methods to get the physical dimensions
		G4double GetW()          		{return W;};
        G4double GetTW()          		{return t_W;};
        G4double GetL()          		{return L;};
        G4double GetS()          		{return S;};
        G4double GetD()          		{return D;};
        G4double GetTD()          		{return t_D;};
        G4double GetHD()          		{return h_D;};
        G4double GetPD()          		{return p_D;};

        G4double GetRodWidth()      {return rod_width;};
        G4double GetRodDepth()      {return rod_depth;};
        G4double GetRodDistance()	{return rod_distance;};

		// Methods to get the geometrical parameters
		G4int GetNX()		        {return nX;};
		G4int GetNY()		        {return nY;};

		// Methods to get the materials
		G4Material* GetCollMaterial()        	{return collMaterial;};


		// Set methods
		// Method to set the world dimensions
		void SetWorldSide(G4double wside)		    {worldSide = wside;};

		// Methods to set the collimator dimensions
		void SetW(G4double a)		{W = a;};
		void SetTW(G4double a)		{t_W = a;};
		void SetL(G4double a)		{L = a;};
        void SetS(G4double a)		{S = a;};
        void SetD(G4double a)		{D = a;};
        void SetTD(G4double a)		{t_D = a;};
        void SetHD(G4double a)		{h_D = a;};
        void SetPD(G4double a)		{p_D = a;};

        void SetRodWidth(G4double a)	{rod_width = a;};
        void SetRodDepth(G4double a)	{rod_depth = a;};
        void SetRodDistance(G4double a)	{rod_distance = a;};

		// Methods to set the geometrical parameters
		void SetNX(G4int nb)		        {nX = nb;};
		void SetNY(G4int nb)                {nY = nb;};
		
		// Methods to set the materials
		void SetCollMaterial                (G4String);
        void SetRodMaterial                 (G4String);
        void SetDetWMaterial                (G4String);
        void SetDetBMaterial                (G4String);
        void SetDetCMaterial                (G4String);

	    // Print a list of parameters
		void PrintParameters();
	
		// Geometry update
		void UpdateGeometry();


	private:
		// Logical volumes
		G4LogicalVolume* experimentalHall_log;
		G4LogicalVolume* coll_log;
        G4LogicalVolume* rod_log;
        G4LogicalVolume* detW_log;
        G4LogicalVolume* detG_log;
        G4LogicalVolume* detB_log;
        G4LogicalVolume* detC_log;

		// Parameterisations
		G4VPVParameterisation* collParam;
        G4VPVParameterisation* detGParam;

		// Physical volumes
		G4VPhysicalVolume* experimentalHall_phys;
		G4VPhysicalVolume* coll_phys;
        G4VPhysicalVolume* rod_phys;
        G4VPhysicalVolume* detW_phys;
        G4VPhysicalVolume* detG_phys;
        G4VPhysicalVolume* detB_phys;
        G4VPhysicalVolume* detC_phys;

		// World dimensions
		G4double worldSide;

		// Geometrical dimensions
		G4double W;
		G4double t_W;
		G4double L;
        G4double S;
        G4double D;
        G4double t_D;
    
        G4double h_D;
        G4double p_D;
    
        G4double rod_width;
        G4double rod_depth;
        G4double rod_distance;

		// Geometrical parameters
        G4int nX;
        G4int nY;
		
		// Materials
		G4Material* defaultMaterial;
		G4Material*	collMaterial;
        G4Material* rodMaterial;
        G4Material*	detWMaterial;
        G4Material*	detBMaterial;
        G4Material*	detCMaterial;

};

#endif

