#ifndef DETECTORHIT_HH
#define DETECTORHIT_HH

#include <vector>
#include <utility>
#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4Types.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "tls.hh"

// General purpose:
//  Represents a hit and contains relevant information about the hit.
//  (The user implementation should provide functionalities which allow to 
//  assign and retrieve hit information).
//  Hit objects can be created in the sensitive detector, and may be used
//  at the end of events to accumulate hit information.
//
// Purpose of the class in this implementation:
//  Represents a hit in the detector, where the relevant hit information is 
//  the energy deposited by the particle and its location


// G4VHit is the abstract base class for creating hit objects.

class DetectorHit : public G4VHit {
	public:
		DetectorHit();							                  // Default constructor
		virtual ~DetectorHit();					// Destructor

		
		// Assignment and comparison operators:  
		const DetectorHit& operator= (const DetectorHit& right);
		int operator==(const DetectorHit& right) const;

		
		// The hit class has user-defined new and delete operators to speed up
		// the generation and deletion of hits objects:
		inline void* operator new(size_t);
		inline void  operator delete(void* hit);

		
		// The G4VHit provides two methods, which can be overloaded by the user to
		// visualize hits or to print information about hits. 
		// Here, these methods are not used (dummy implementation):
		virtual void Draw() { }
		virtual void Print() { }

    
		
		inline void SetX(G4double t) 			  {hitX = t;}
		inline G4double GetX() 					  {return hitX;}
        inline void SetY(G4double t) 			  {hitY = t;}
        inline G4double GetY() 					  {return hitY;}


	private:
		G4double hitX;
        G4double hitY;
	
};


typedef G4THitsCollection<DetectorHit> DetectorHitsCollection;
extern G4ThreadLocal G4Allocator<DetectorHit>* DetectorHitAllocator;


inline void* DetectorHit::operator new(size_t) {
    // Implementation of the new operator
    if(!DetectorHitAllocator) DetectorHitAllocator = new G4Allocator<DetectorHit>;
    return (void*) DetectorHitAllocator->MallocSingle();
}


inline void DetectorHit::operator delete(void* hit) {
    // Implementation of the delete operator
    DetectorHitAllocator->FreeSingle((DetectorHit*) hit);
}

#endif // DETECTORHIT_HH
